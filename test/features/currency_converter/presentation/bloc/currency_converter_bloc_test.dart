import 'package:currency_converter/features/currency_converter/domain/entities/currency_converter_entity.dart';
import 'package:currency_converter/features/currency_converter/presentation/bloc/currency_converter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  //Arrange
  var currencyConverterBloc = CurrencyConverterBloc();
  currencyConverterBloc.initData();
  currencyConverterBloc.addNewCurrencyEntityIntoListCurrencies(const CurrencyEntity(
    currencyCode: "SGD",
    proportionWithUSD: 1.36,
  ));

  group("Handle Logic Currency converter ", () {
    test("Get the proportion when converted to 1USD When Selected a item", () {
      //Act
      var result = currencyConverterBloc
          .getProportionWithUSDWhenSelectedItemInList('VND');
      //Assert
      expect(24301.0, result);
    });

    test("Get the proportion When Selected 2 currencies", () {
      //Act
      var result = currencyConverterBloc
          .getProportionWhenSelectedTwoCurrencies("SGD", "VND");
      //Assert
      expect(17868.38, result);
    });


  });
}
