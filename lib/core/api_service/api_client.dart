// import 'dart:convert';
// import 'package:dio/dio.dart';
//
//
// abstract class APIClient {
//   Future<Response> get(Map<String, dynamic> queryParameters);
// }
//
// class OpenWeatherClient extends APIClient{
//   OpenWeatherClient.internal();
//
//   static final share = OpenWeatherClient.internal();
//
//   factory OpenWeatherClient() => share;
//
//   @override
//   Future<Response> get(Map<String, dynamic> queryParameters) async {
//     const path = "https://openweathermap.org/data/2.5/find";
//     var dio = Dio();
//     try {
//       return await dio.get(path, queryParameters: queryParameters);
//     } catch (error) {
//       return Response(requestOptions: RequestOptions());
//     }
//   }
// }



