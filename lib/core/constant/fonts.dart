//
// import 'package:flutter/material.dart';
// import 'package:weather_app/core/constant/text_style.dart';
//
// const String DEFAULT_FONT_NAME = 'Roboto';
//
// const TextStyle regFont = TextStyle(fontFamily: DEFAULT_FONT_NAME);
// const TextStyle medFont = TextStyle(fontFamily: DEFAULT_FONT_NAME, fontWeight: FontWeight.w500);
// const TextStyle boldFont = TextStyle(fontFamily: DEFAULT_FONT_NAME, fontWeight: FontWeight.w700);
//
//
// // Invert
// final TextStyle Title_30_Bold_Invert = boldFont.withSdds(typography: SddsTextTypography.title, size: 30, type: SddsTextType.invert);
// final TextStyle Title_20_Med_Invert = medFont.withSdds(typography: SddsTextTypography.title, size: 20, type: SddsTextType.invert);
// final TextStyle Body_16_Bold_Invert = boldFont.withSdds(typography: SddsTextTypography.body, size: 16, type: SddsTextType.invert);
//
//
