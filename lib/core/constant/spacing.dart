/// **0**
const double spacing_zero = 0.0;

/// **2**
const double spacing_s = 2.0;

/// **4**
const double spacing_xs = 4.0;

/// **6**
const double spacing_xxs = 6.0;

/// **8**
const double spacing_sm = 8.0;

/// **10**
const double spacing_xm = 10.0;

/// **12**
const double spacing_md = 12.0;

/// **16**
const double spacing_lg = 16.0;

/// **24**
const double spacing_xl = 24.0;

/// **32**
const double spacing_2xl = 32.0;

/// **48**
const double spacing_3xl = 48.0;

/// **64**
const double spacing_4xl = 64.0;