// import 'package:flutter/material.dart';
// import 'package:weather_app/core/constant/colors.dart';
//
// enum SddsTextTypography { legendary, hero, display, title, body, caption, sponsor }
//
// enum SddsTextType {
//   invert,
//   sub,
//   defaulT,
//   disable,
//   highlight,
//   link,
//   linkDisable,
//   linkVisited,
//   linkPressed,
//   error,
//   warning,
//   success,
//   strikethrough
// }
//
// extension GetColor on SddsTextTypography {
//   Color color(SddsTextType type) {
//     switch (this) {
//       case SddsTextTypography.legendary:
//         return _colorForLegendary(type);
//       case SddsTextTypography.hero:
//         return _colorForHero(type);
//       case SddsTextTypography.display:
//         return _colorForDisplay(type);
//       case SddsTextTypography.title:
//         return _colorForTitle(type);
//       case SddsTextTypography.body:
//         return _colorForBody(type);
//       case SddsTextTypography.caption:
//         return _colorForCaption(type);
//       case SddsTextTypography.sponsor:
//         return _colorForSponsor(type);
//       default:
//         return color_black;
//     }
//   }
//
//   Color _colorForLegendary(SddsTextType type) {
//     switch (type) {
//       case SddsTextType.invert:
//         return color_white;
//       case SddsTextType.sub:
//         return color_gray_700;
//       case SddsTextType.disable:
//         return color_gray_200;
//       default:
//         return color_gray_900;
//     }
//   }
//
//   Color _colorForHero(SddsTextType type) {
//     switch (type) {
//       case SddsTextType.invert:
//         return color_white;
//       case SddsTextType.sub:
//         return color_gray_700;
//       case SddsTextType.disable:
//         return color_gray_200;
//       default:
//         return color_gray_900;
//     }
//   }
//
//   Color _colorForDisplay(SddsTextType type) {
//     switch (type) {
//       case SddsTextType.invert:
//         return color_white;
//       case SddsTextType.sub:
//         return color_gray_700;
//       case SddsTextType.disable:
//         return color_gray_200;
//       default:
//         return color_gray_900;
//     }
//   }
//
//   Color _colorForTitle(SddsTextType type) {
//     switch (type) {
//       case SddsTextType.invert:
//         return color_white;
//       case SddsTextType.sub:
//         return color_gray_700;
//       case SddsTextType.disable:
//         return color_gray_200;
//       case SddsTextType.highlight:
//         return color_red_500;
//       default:
//         return color_gray_900;
//     }
//   }
//
//   Color _colorForBody(SddsTextType type) {
//     switch (type) {
//       case SddsTextType.invert:
//         return color_white;
//       case SddsTextType.sub:
//         return color_gray_500;
//       case SddsTextType.disable:
//         return color_gray_200;
//       case SddsTextType.highlight:
//         return color_red_500;
//       case SddsTextType.link:
//         return color_ocean_blue_400;
//       case SddsTextType.linkDisable:
//         return color_ocean_blue_100;
//       case SddsTextType.linkVisited:
//         return color_purple_600;
//       case SddsTextType.linkPressed:
//         return color_blue_600;
//       case SddsTextType.error:
//         return color_red_500;
//       case SddsTextType.warning:
//         return color_orange_500;
//       case SddsTextType.success:
//         return color_green_500;
//       default:
//         return color_gray_700;
//     }
//   }
//
//   Color _colorForCaption(SddsTextType type) {
//     switch (type) {
//       case SddsTextType.invert:
//         return color_white;
//       case SddsTextType.sub:
//         return color_gray_500;
//       case SddsTextType.disable:
//         return color_gray_200;
//       case SddsTextType.highlight:
//         return color_red_500;
//       case SddsTextType.link:
//         return color_ocean_blue_400;
//       case SddsTextType.linkDisable:
//         return color_ocean_blue_100;
//       case SddsTextType.linkVisited:
//         return color_purple_600;
//       case SddsTextType.linkPressed:
//         return color_blue_600;
//       case SddsTextType.error:
//         return color_red_500;
//       case SddsTextType.warning:
//         return color_orange_500;
//       case SddsTextType.success:
//         return color_green_500;
//       case SddsTextType.strikethrough:
//         return color_gray_200;
//       default:
//         return color_gray_700;
//     }
//   }
//
//   Color _colorForSponsor(SddsTextType type) {
//     switch (type) {
//       case SddsTextType.sub:
//         return color_gray_500;
//       default:
//         return color_gray_700;
//     }
//   }
// }
//
// extension Sdds on TextStyle {
//   TextStyle withSdds({required SddsTextTypography typography, required double size, required SddsTextType type}) {
//     return copyWith(
//       fontSize: size,
//       color: typography.color(type),
//       height: _getHeight(size),
//       letterSpacing: _getLetterSpacing(size),
//     );
//   }
//
//   /// height =  Line height (design) / Font size
//   double _getHeight(double fontSize) {
//     if (fontSize == 36) {
//       return 44 / fontSize;
//     } else if (fontSize == 32) {
//       return 38 / fontSize;
//     } else if (fontSize == 24) {
//       return 32 / fontSize;
//     } else if (fontSize == 20) {
//       return 22 / fontSize;
//     } else if (fontSize == 16) {
//       return 22 / fontSize;
//     } else if (fontSize == 14) {
//       return 18 / fontSize;
//     } else if (fontSize == 12) {
//       return 16 / fontSize;
//     } else if (fontSize == 11) {
//       return 12 / fontSize;
//     } else if (fontSize == 10) {
//       return 12 / fontSize;
//     }
//     return 18 / fontSize;
//   }
//
//   double _getLetterSpacing(double fontSize) {
//     if (fontSize >= 20) {
//       return 0.15;
//     }
//     return 0;
//   }
// }