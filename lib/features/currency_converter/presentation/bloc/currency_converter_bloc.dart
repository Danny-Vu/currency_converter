import 'package:currency_converter/features/currency_converter/domain/entities/currency_converter_entity.dart';

class CurrencyConverterBloc {
  /*
  Input:
      - Init a list with the default has USA and VND
      - Currency Code:
              Example: CAD -> Canada dollar
      - Proportion when convert to 1USD:
              Example: USD - VND: 1 - 24.301
      - Be able to change number of money of the first selected item

      - Swap 2 currencies
  Output:
      - There are two lists to select two currencies UI
      - Show proportion
                Example: USD - SGD: 1 - 1.36
      - Show number of money of the second selected items

      - Swap their currencies
   */
  final Set<CurrencyEntity> _listCurrencies = {};


  double proportionOfTwoCurrency = 0.0;

  Set<CurrencyEntity> get listCurrencies => _listCurrencies;

  // Input

  void initData() {
    _listCurrencies.add(const CurrencyEntity(
      currencyCode: "USD",
      proportionWithUSD: 1.0,
    ));
    _listCurrencies.add(const CurrencyEntity(
      currencyCode: "VND",
      proportionWithUSD: 24301.0,
    ));
  }

  void addNewCurrencyEntityIntoListCurrencies(
      CurrencyEntity currencyEntity) {
    _listCurrencies.add(currencyEntity);
  }

  double getProportionWithUSDWhenSelectedItemInList(
      String selectedCurrency) {
    double proportion = 0.0;
    for (var item in _listCurrencies) {
      if (item.currencyCode == selectedCurrency) {
        proportion = item.proportionWithUSD;
      }
    }
    return proportion;
  }

  double getProportionWhenSelectedTwoCurrencies(
      String firstSelectedCurrency, String secondSelectedCurrency) {
    double firstProportion = getProportionWithUSDWhenSelectedItemInList(firstSelectedCurrency);
    double secondProportion = getProportionWithUSDWhenSelectedItemInList(secondSelectedCurrency);
    return double.tryParse((secondProportion/firstProportion).toStringAsFixed(2) ?? '0.0') ?? 0.0;
  }

}
