

import 'package:currency_converter/core/utils/parse_model.dart';

class ProportionResponse {
  final String? comparedCountryId;
  final String? rate;

  ProportionResponse({
    this.comparedCountryId,
    this.rate,
});

  factory ProportionResponse.fromJson(Map<String, dynamic> json) {
    return ProportionResponse(
      comparedCountryId: json.parseString('comparedCountryId'),
      rate: json.parseString('rate'),
    );
  }
}