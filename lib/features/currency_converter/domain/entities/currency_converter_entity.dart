import 'package:equatable/equatable.dart';

class CurrencyEntity extends Equatable{
  final String currencyCode;
  final double proportionWithUSD;

  const CurrencyEntity({
    this.currencyCode = '',
    this.proportionWithUSD = 0.0,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [currencyCode, proportionWithUSD];
}
